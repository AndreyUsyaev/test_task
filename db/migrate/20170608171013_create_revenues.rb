class CreateRevenues < ActiveRecord::Migration[5.1]
  def change
    create_table :revenues do |t|
      t.references :good, index: true
      t.float :revenue
      t.datetime :date

      t.timestamps
    end
  end
end
